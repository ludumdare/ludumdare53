extends Node2D

func _process(delta):
	
	ECS.update()
	


func _on_land_button_down():
	
	var pigeon = $Pigeon.get_component("pigeon") as PigeonComponent
	pigeon.state = pigeon.PigeonStates.LAND


func _on_strut_button_down():
	var pigeon = $Pigeon.get_component("pigeon") as PigeonComponent
	pigeon.state = pigeon.PigeonStates.STRUT


func _on_jump_button_down():
	var pigeon = $Pigeon.get_component("pigeon") as PigeonComponent
	pigeon.state = pigeon.PigeonStates.JUMP


func _on_fly_button_down():
	var pigeon = $Pigeon.get_component("pigeon") as PigeonComponent
	pigeon.state = pigeon.PigeonStates.FLY
