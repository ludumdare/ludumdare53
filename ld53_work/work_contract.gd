extends Node2D

func _ready():
	Global.show_contract.connect(on_show_contract)	
	
func on_show_contract(contract: ContractResource, index, total, pigeons):
	$Contract/Type.text = "Type : %s" % contract.type
	$Contract/Success.text = "Success Chance : %s" % contract.difficulty_value
	$Contract/Distance.text = "Approx Distance : %d km" % contract.distance
	$Contract/Pigeons.text = "Pigeons Needed : %d " % contract.pigeons
	$Contract/Expire.text = "Days To Expire : %d " % contract.days_to_expire
	$Contract/Payment.text = "$%d" % contract.payment
	$Contract/Reward.text = "$%d" % contract.reward
	$Contract/Influence.text = "%d" % contract.victory_points
	$Contract/Showing.text = "%d of %d" % [index + 1, total]
	
	$Contract/More.text = "" 
	$Contract/Ending.text = ""
	
	if contract.pigeons > pigeons:
		$Contract/More.text = "Need More Pigeons"
		
	if contract.days_to_expire < 3:
		$Contract/Ending.text = "Ending Soon"
	
func _on_accept_button_down():
	Global.accept_contract.emit()


func _on_next_button_down():
	Global.next_contract.emit()
	Game.play_sound("Page")
