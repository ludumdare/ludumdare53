extends Node2D


func _on_play_button_down():
	ECS.clean()
	ECS.update()
	Game.set_state(Game.GameStates.PLAY)


func _process(delta):
	ECS.update()


func _on_quit_button_down():
	ECS.clean()
	ECS.update()
	Game.set_state(Game.GameStates.QUIT)


func _on_about_button_down():
	ECS.clean()
	ECS.update()
	Game.set_state(Game.GameStates.ABOUT)
