extends Node2D

@export var starting_pigeons: int = 100
@export var starting_food: int = 500
@export var starting_currency: int = 100
@export var starting_pigeon_capacity: int = 0
@export var starting_food_storage: int = 1000
@export var pigeon_speed: int = 20 # km/h
@export var transport_speed: int = 60 # km/h
@export var starting_influence: int = 10
@export var node_loft: PackedScene
@export var node_loft_top: PackedScene
@export var node_upgrade: PackedScene
@export var node_coop: PackedScene

var __current_state: int 
var __next_state: int 
var __last_state: int

var __contract_ui: Control
var __win_ui: Control
var __lose_ui: Control
var __shop_ui: Control
var __menu_ui: Control
var __hide_ui: Control
var __play_ui: Control

var __game_data: GameResource

var __open_contracts: Array

var __contract_index: int

var total_coops: int = 0
var total_lofts: int = 0

var lofta1
var lofta2
var loftat
var loftb1
var loftb2
var loftbt


var visit_contract: bool = true 
var visit_shop: bool = true

func _ready():
	
	Logger.trace("[Play] _ready")
	
	Game.play_music("Play")
	
	__current_state = Global.PlayStates.INIT
	
	$UI/PLAY/ContractsButton.button_down.connect(__on_contracts_button__)
	$UI/PLAY/ShopButton.button_down.connect(__on_shop_button__)
	$UI/PLAY/MenuButton.button_down.connect(__on_menu_button__)
	$UI/HIDE/ExitButton.button_down.connect(__on_exit_button__)
	
	Global.next_day.connect(__on_next_day__)
	Global.next_day_stage.connect(__on_next_day_stage__)
	Global.next_contract.connect(on_next_contract)
	Global.accept_contract.connect(on_accept_contract)
	Global.contract_success.connect(on_contract_success)
	Global.contract_failure.connect(on_contract_failure)
	
	__contract_ui = $UI/CONTRACT
	__menu_ui = $UI/MENU
	__win_ui = $UI/WIN
	__lose_ui = $UI/LOSE
	__shop_ui = $UI/SHOP
	__play_ui = $UI/PLAY
	__hide_ui = $UI/HIDE
	
	print(Game.get_user_setting("hello"))
	
	if Game.get_user_setting("hello") != "1":
		Game.user_settings["hello"] = "1"
		Global.new_message.emit("Thanks for playing my game.")
		Global.new_message.emit("The goal is to have 100% influence.")
		Global.new_message.emit("Influence is gained when contracts are successful.")
		Global.new_message.emit("Influence is lost when contracts fail or expire.")
		Global.new_message.emit("Use the shop to purchase more pigeons, food or other upgrades.")
		Global.new_message.emit("Good luck and have fun! -- PAUL --")
	

func _process(_delta: float):
	
	Logger.fine("[Play] _process")
	
	match __current_state:
		
		Global.PlayStates.INIT:
			__on_init__()
			
		Global.PlayStates.CONTRACT:
			__on_contract__()
			
		Global.PlayStates.SHOP:
			__on_shop__()
			
		Global.PlayStates.MENU:
			__on_menu__()
			
		Global.PlayStates.START:
			__on_start__()
			
		Global.PlayStates.PLAY:
			__on_play__()
			
		Global.PlayStates.QUIT:
			__on_quit__()
			
		Global.PlayStates.LOSE:
			__on_lose__()
			
		Global.PlayStates.WIN:
			__on_win__()
			
			
	if __next_state != __current_state:
		__last_state = __current_state
		__current_state = __next_state
		
	__update_ui__()
		
		
func __update_ui__():
	$UI/PLAY/Days.text = "DAY : %d" % __game_data.current_day 
	$UI/PLAY/Influence.text = "INFLUENCE: %%%d" % __game_data.victory_points
	$UI/SHOP/TextureRect/Cash.text = "Cash : $%s" % Global.format_commas(str(__game_data.currency))
	$UI/PLAY/Stage.text = "STAGE : %d" % __game_data.current_stage
	$UI/PLAY/Progress.value = __game_data.current_stage
	Global.update_ui.emit(__game_data)
		
func __on_init__():
	Logger.trace("[Play] __on_init__")
	__set_state__(Global.PlayStates.START)
	__hide__ui__()
	
	# set some default values from our external vars
	__game_data = GameResource.new()
	__game_data.pigeons = starting_pigeons
	__game_data.food = starting_food 
	__game_data.currency = starting_currency
	__game_data.capacity = starting_pigeon_capacity
	__game_data.victory_points = starting_influence
	__game_data.current_day = 1
	
	# create easy and 2 med contracts to start
	
	for i in range(4):
		__game_data.contracts.append(Global.create_contract(0))
		
	for i in range(1):
		__game_data.contracts.append(Global.create_contract(1))
		
	add_loft()
		
	__update_contract_information__()
						
	
func __on_contract__():
	Logger.trace("[Play] __on_contract__")
	__show__ui__("contract")
	
	__open_contracts.clear()
	
	for i in range(__game_data.contracts.size()):
	
		var contract = __game_data.contracts[i] as ContractResource
	
		if contract.open:
			__open_contracts.append(contract)

	__contract_index = 0
	if __open_contracts.size() > 0:
		Global.show_contract.emit(__open_contracts[__contract_index], __contract_index, __open_contracts.size(), __game_data.pigeons)
		__set_state__(Global.PlayStates.IDLE)
	else:
		__show__ui__("play")
		__set_state__(Global.PlayStates.PLAY)
	
	
func __on_shop__():
	Logger.trace("[Play] __on_shop__")
	__show__ui__("shop")
	__set_state__(Global.PlayStates.IDLE)
	
	
func __on_menu__():
	Logger.trace("[Play] __on_menu__")
	__show__ui__("menu")
	__set_state__(Global.PlayStates.IDLE)
	
	
func __on_start__():
	Logger.trace("[Play] __on_start__")
	__show__ui__("play")
	__set_state__(Global.PlayStates.PLAY)
	
	
func __on_play__():
	Logger.fine("[Play] __on_play__")
	ECS.update("standard") 
	
	
func __on_quit__():
	Logger.trace("[Play] __on_quit__")
	
	
func __on_lose__():
	Logger.trace("[Play] __on_lose__")
	__show__ui__("lose")
	__set_state__(Global.PlayStates.IDLE)
	

func __on_win__():
	Logger.trace("[Play] __on_win__")
	__show__ui__("win")
	__set_state__(Global.PlayStates.IDLE)
	

func __set_state__(state: int):
	Logger.trace("[Play] __set_state__")
	__next_state = state


func __on_contracts_button__():
	Logger.trace("[Play] __on_contracts_button__")
	__set_state__(Global.PlayStates.CONTRACT)
	$UI/PLAY/ContractsButton/Attention.hide()
	$UI/PLAY/MessagePanel.paused = true
	visit_contract = true


func __on_shop_button__():
	Logger.trace("[Play] __on_shop_button__")
	__set_state__(Global.PlayStates.SHOP)
	$UI/PLAY/MessagePanel.paused = true
	$UI/PLAY/ShopButton/Attention.hide()
	visit_shop = true


func __on_menu_button__():
	Logger.trace("[Play] __on_menu_button__")
	__set_state__(Global.PlayStates.MENU)
	$UI/PLAY/MessagePanel.paused = true
	
	
func __on_exit_button__():
	Logger.trace("[Play] __on_exit_button__")
	Global.exit_button.emit()
	__update_contract_information__()
	__set_state__(Global.PlayStates.START)
	Game.play_sound("Click")
	$UI/PLAY/MessagePanel.paused = false


func __hide__ui__():
	Logger.trace("[Play] __hide_ui__")
	__hide_ui.hide()
	__contract_ui.hide()
	__menu_ui.hide()
	__win_ui.hide()
	__lose_ui.hide()
	__shop_ui.hide()
	

func __on_next_day__(day):
	Logger.trace("[Play] __on_next_day__")
	
	__game_data.current_day = day
	__game_data.current_stage = 0
	
	# cleanup contracts
		
	for i in range(__game_data.contracts.size()):
		
		var contract = __game_data.contracts[i] as ContractResource
		
		if contract.open:
			
			# update days to expire
			
			contract.days_to_expire -= 1
			
			# remove expired contract and update stats
			
			Logger.fine("days to expire %d" % contract.days_to_expire)
			
			if contract.days_to_expire <= 0:
				contract.expired = true
				contract.active = false
				contract.failed = true
				contract.open = false
				Global.new_message.emit("A contract has expired. %d Influence lost." % contract.victory_points)
				__game_data.victory_points -= contract.victory_points

	for i in range(__game_data.contracts.size()):
		
		var contract = __game_data.contracts[i] as ContractResource
		
		# only check active contracts
		
		if contract.active:
			
			if contract.days_travel_left > 0:
				
				contract.days_travel_left -= 1
				
				if contract.days_travel_left <= 0:
					contract.days_travel_left = 0
			
			else:
				
				# days of travel left
				# pigeons actually trave 20-30 mph
				# but for the game we will do 20-30 per day
				
				contract.days_return_left -= 1
				
				if contract.days_return_left <= 0:
					Global.contract_success.emit(contract)
				
				# pigeons remaining
				# the higher the difficulty the more chance that a pigeon will die
				# there is always 20% chance that one pigeon will die
				
				var _dead_chance = randi_range(0,9)
				if _dead_chance < 2:
					contract.pigeons_left -= 1
				
				# there is an additional 30% for 2 & 3
				
				if contract.difficulty > 1:
					_dead_chance = randi_range(0,9)
					if _dead_chance < 3:
						contract.pigeons_left -= 1
					
				if contract.difficulty > 2:
					_dead_chance = randi_range(0,9)
					if _dead_chance < 3:
						contract.pigeons_left -= 1
				
				# and finally there is an additional 50% for the hardest level

				if contract.difficulty > 3:
					_dead_chance = randi_range(0,9)
					if _dead_chance < 5:
						contract.pigeons_left -= 1
						
				# check if there are any pigeons left
				
				if contract.pigeons_left <= 0:
					Global.contract_failure.emit(contract)
								
				# update active contract ccounter
				
				__game_data.contracts_active += 1
				
		else:
			
			__game_data.contracts_available += 1
			
	__update_contract_information__()
						
	# check for lose conditions
	
	if __game_data.victory_points <= 0:
		__game_data.victory_points = 0
		__set_state__(Global.PlayStates.LOSE)
		
	# check for win conditions
	
	if __game_data.victory_points >= 100:
		__game_data.victory_points = 100
		__set_state__(Global.PlayStates.WIN)
	

func __on_next_day_stage__(stage):
	
	Logger.trace("[Play] __on_next_day_stage__")
	
	__game_data.current_stage = stage

	# calc pigeons that have passed
	
	# how many pigeons over capacity are we?
	
	var _pigeons_over_capacity = __game_data.pigeons - __game_data.capacity
	
	# at least 1 plus 1% of affected pigeons will die
	if _pigeons_over_capacity > 0:
		var _pigeons_dead = int(_pigeons_over_capacity * 0.01) + 1
		
	# how many pigeons can we feed for the next stage
	# pigeons eat 2% of a food ration per pigeon
	
	var _pigeons_we_can_feed = int(__game_data.food / 0.02)
	var _pigeons_affected = __game_data.pigeons - _pigeons_we_can_feed

	# at least 1 plus 15-20% of affected pigeons will die from starvation
		
	if _pigeons_affected > 0:
		var _pigeons_starved = int(_pigeons_affected * randf_range(0.15,0.20)) + 1
		__game_data.pigeons -= _pigeons_starved
		
	if __game_data.food < 10:
		__game_data.pigeons -= 1

	# calc pigeons born
	# only 1%-5% of pigeons have babies
	
	var _pb = int(ceil(__game_data.pigeons * randf_range(0.01,0.02)))
	__game_data.pigeons += _pb
	
	if __game_data.pigeons <= 0:
		__game_data.pigeons = 0
	
	# calc food remaining
	
	var _food_consumed = int(ceil(__game_data.pigeons * 0.02))
	__game_data.food -= _food_consumed
	if __game_data.food < 0:
		__game_data.food = 0
		if visit_shop:
			Global.new_message.emit("Your pigeons need food. Visit the shop!")
			$UI/PLAY/ShopButton/Attention.show()
			visit_shop = false
	
	# calc capacity available
	
	# 30% chance of getting a new contract
	
	var chance = randi_range(0,10)
	if chance < 3:
		var c: ContractResource = Global.create_contract()
		__game_data.contracts.append(c)
		$UI/PLAY/ContractsButton/Attention.show()
		if visit_contract:
			Global.new_message.emit("A new contract is available . . .")
			visit_contract = false
			
			
	__update_contract_information__()
	

func on_next_contract():
	__contract_index += 1
	
	if __contract_index > __open_contracts.size() - 1:
		__contract_index = 0 
		
	Global.show_contract.emit(__open_contracts[__contract_index], __contract_index, __open_contracts.size(), __game_data.pigeons)

	
func on_accept_contract():
	var _contract = __open_contracts[__contract_index] as ContractResource
	if __game_data.pigeons >= _contract.pigeons:
		Game.play_sound("Click")
		_contract.active = true
		_contract.open = false
		__game_data.currency += _contract.payment
		__game_data.pigeons -= _contract.pigeons
		__set_state__(Global.PlayStates.CONTRACT)
		__update_contract_information__()
	else:
		Game.play_sound("Invalid")

func __show__ui__(name):
	Logger.trace("[Play] __show_ui__")
	
	__hide__ui__()
		
	match name:
		
		"contract":
			__hide_ui.show()
			__contract_ui.show()
			
		"menu":
			__hide_ui.show()
			__menu_ui.show()
			
		"win":
			__win_ui.show()
			
		"lose":
			__lose_ui.show()
			
		"shop":
			__hide_ui.show()
			__shop_ui.show()
			
		"play":
			__play_ui.show()
	


func _on_home_button_down():
	ECS.clean()
	ECS.update()
	Game.set_state(Game.GameStates.HOME)
	Game.play_sound("Click")
	Game.play_music("Home")


func _on_replay_button_down():
	ECS.clean()
	ECS.update()
	Game.set_state(Game.GameStates.PLAY)
	Game.play_sound("Click")


func on_contract_success(contract: ContractResource):
	contract.active = false
	contract.success = true
	__game_data.victory_points += contract.victory_points
	__game_data.currency += contract.reward
	__game_data.pigeons += contract.pigeons_left
	Logger.info("Success!")
	Global.new_message.emit("Your pigeons returned! %d Influence added." % contract.victory_points)
	
	
func on_contract_failure(contract: ContractResource):
	contract.active = false
	contract.failed= true
	Logger.info("Failure!")
	Global.new_message.emit("Your pigeons did not return. %d Influence lost." % contract.victory_points)


func __update_contract_information__():
	
	# update contract information
	
	__game_data.contracts_available = 0
	__game_data.contracts_active = 0
	__game_data.contracts_expired = 0
	__game_data.contracts_failure = 0
	__game_data.contracts_success = 0
	
	for i in range(__game_data.contracts.size()):
		
		var contract = __game_data.contracts[i] as ContractResource
		
		if contract.open:
			__game_data.contracts_available += 1
			
		if contract.active:
			__game_data.contracts_active += 1
			
		if contract.expired:
			__game_data.contracts_expired += 1
			
		if contract.success:
			__game_data.contracts_success += 1
			
		if contract.failed:
			__game_data.contracts_failure += 1


func _on_pigeons_10_button_down():
	if __game_data.currency >= 25:
		Game.play_sound("Click")
		__game_data.pigeons += 10
		__game_data.currency -= 25
	else:
		Game.play_sound("Invalid")
		


func _on_food_50_button_down():
	if __game_data.currency >= 50:
		Game.play_sound("Click")
		__game_data.food += 50
		__game_data.currency -= 50
	else:
		Game.play_sound("Invalid")
		


func _on_coop_1_button_down():
	if __game_data.currency >= 100 and total_coops < 4:
		Game.play_sound("Click")
		add_coop()
		__game_data.currency -= 100
	else:
		Game.play_sound("Invalid")
		


func _on_loft_1_button_down():
	if __game_data.currency >= 250 and total_lofts < 4:
		Game.play_sound("Click")
		add_loft()
		__game_data.currency -= 250
	else:
		Game.play_sound("Invalid")
		

func add_loft():
	
	total_lofts += 1
	__game_data.capacity += 50
	
	if total_lofts == 1:
		lofta1 = node_loft.instantiate()
		loftat = node_loft_top.instantiate()
		$Entities/Lofts.add_child(lofta1)
		lofta1.position = Vector2.ZERO
		$Entities/Lofts.add_child(loftat)
		loftat.position = Vector2(0,-120)
	
	if total_lofts == 2:
		loftb1 = node_loft.instantiate()
		loftbt = node_loft_top.instantiate()
		$Entities/Lofts.add_child(loftb1)
		loftb1.position = Vector2(192,0)
		$Entities/Lofts.add_child(loftbt)
		loftbt.position = Vector2(192,-120)
		
	if total_lofts == 3:
		lofta2 = node_upgrade.instantiate()
		$Entities/Lofts.add_child(lofta2)
		lofta2.position = Vector2(0,-120)
		loftat.position = Vector2(0,-240)
		
	if total_lofts == 4:
		loftb2 = node_upgrade.instantiate()
		$Entities/Lofts.add_child(loftb2)
		loftb2.position = Vector2(192,-120)
		loftbt.position = Vector2(192,-240)
		
	
	
func add_coop():
	
	total_coops += 1
	__game_data.capacity += 20
	
	var coop = node_coop.instantiate() as Node2D
	$Entities/Coops.add_child(coop)
	coop.position = Vector2(1,0) * ((total_coops - 1) * 48)
