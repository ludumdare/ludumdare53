class_name MoveComponent
extends Component

@export var direction: Vector2 
@export var speed: float = 60
@export var speed_factor: float = 1.0
