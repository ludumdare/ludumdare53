class_name CloudsComponent
extends Component

@export var max_clouds: int = 3
@export var max_pregen: int = 10

var first: bool = true
