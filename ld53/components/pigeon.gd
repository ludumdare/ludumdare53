class_name PigeonComponent
extends Component 

enum PigeonStates 
{
	NONE,
	WAIT,
	IDLE,
	FLY,
	FLYING,
	STRUT,
	STRUTTING,
	EAT,
	LAND,
	JUMP,
	JUMPING,
	TURN,
}

var state: int = PigeonStates.IDLE
var perched: bool = false # stationary but not on the ground
var grounded: bool = false # on the ground
