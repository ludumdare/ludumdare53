class_name TickActionComponent
extends Component

# how many ticks need to pass before the action is triggered
@export var ticks_per_action : int = 5

var tick : int = 0
var last_tick : int = 0
var actions : int = 0
var has_action : bool = false
