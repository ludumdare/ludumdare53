class_name DayActionComponent
extends Component

@export var actions_per_stage: int = 3600
@export var stages_per_day: int = 12

var stage_of_day: int = 0
var actions: int = 0
var days: int = 0
