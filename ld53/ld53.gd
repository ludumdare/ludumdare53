extends Node

@export var window_manager: WindowManagerResource

func _ready():
	
	WindowManager.load(window_manager)
	LudumDare.start_game("res://ld53/ld53.ini")
	Game.play_music("Home")
	Game.play_sound("Pigeons")
