class_name PigeonEntity
extends Entity


func _on_area_entered(area: Area2D):
	
	var _collision = get_component("collision") as CollisionComponent
	
	if _collision == null:
		return
		
	_collision.has_collided = true 
	_collision.area = area
	
	if area.name.to_lower().contains("ground"):
		remove_component("gravity")
		var _move = get_component("move") as MoveComponent
		_move.direction = Vector2.ZERO
		var _pigeon = get_component("pigeon") as PigeonComponent
		_pigeon.grounded = true
		_pigeon.perched = false
		_pigeon.state = _pigeon.PigeonStates.IDLE
