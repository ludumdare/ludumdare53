class_name CloudEntity
extends Entity


func on_ready():
	
	var scales = [ Vector2(1,1), Vector2(-1,1), Vector2(1, -1), Vector2(-1,-1) ] 
	var _scale = scales[randi_range(0,3)]
	self.scale = _scale
	var _ypos = randi_range(0, 220)
	self.global_position += Vector2(-100, _ypos)
	var _anim = get_node("AnimatedSprite2D") as AnimatedSprite2D
	_anim.frame = randi_range(0, 4)
	var _move = get_component("move") as MoveComponent
	_move.direction = Vector2.RIGHT
	_move.speed_factor += randf_range(-0.25,0.25)

