class_name GameResource
extends Resource

var current_day: int 
var current_stage: int 
var pigeons: int 
var food: int 
var storage: int
var victory_points: int 
var currency: int 
var capacity: int 
var scenario: ScenarioResource
var contracts: Array
var contracts_available: int 
var contracts_active: int
var contracts_success: int
var contracts_failure: int
var contracts_expired: int
