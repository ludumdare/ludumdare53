class_name ContractResource
extends Resource

var type: String
var distance: int
var days_travel: int 
var days_travel_left: int
var days_return: int
var days_return_left: int
var pigeons: int
var pigeons_sent: int
var pigeons_left: int
var difficulty: int 
var difficulty_value: String
var victory_points: int
var reward: int 
var odds: int
var payment: int
var adjusted_odds: int
var training_days: int
var days_to_expire: int
var open: bool = true
var expired: bool = false
var success: bool = false
var failed: bool = false
var active: bool = false
