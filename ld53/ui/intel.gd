extends Node2D

func _ready():
	
	Logger.trace("[Intel] _ready")	
	
	Global.update_ui.connect(__on_update_ui__)
	

func __on_update_ui__(game_data: GameResource):
	
	Logger.fine("[Intel] __on_update_ui__")	
	
	$Data/Pigeons.text = "Pigeons : %s" % Global.format_commas(str(game_data.pigeons))
	$Data/Food.text = "Food : %s" % Global.format_commas(str(game_data.food))
	$Data/Cash.text = "Cash : $ %s" % Global.format_commas(str(game_data.currency))
	$Data/Capacity.text = "Capacity : %s" % Global.format_commas(str(game_data.capacity))
	$Data/Storage.text = "Storage : %s" % Global.format_commas(str(game_data.storage))
