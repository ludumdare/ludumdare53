extends Node2D

func _ready():
	
	Logger.trace("[Intel] _ready")	
	
	Global.update_ui.connect(__on_update_ui__)

func __on_update_ui__(game_data: GameResource):
	
	Logger.fine("[Intel] __on_update_ui__")
	
	$Data/Available.text = "Open : %d" % game_data.contracts_available
	$Data/Active.text = "Active : %d" % game_data.contracts_active
	$Data/Success.text = "Success : %d" % game_data.contracts_success
	$Data/Failure.text = "Failed : %d" % game_data.contracts_failure
