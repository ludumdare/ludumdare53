extends Panel

var messages: Array
var showing: bool
var paused: bool

func _ready():

	Global.new_message.connect(on_new_message)
	
	
func on_new_message(message: String):
	Logger.info("[MessagePanel] on_new_message")
	messages.append(message)


func _on_timer_timeout():
	Logger.info("[MessagePanel] timer")
	
	if paused:
		return
	
	if showing:
		return
		
	if messages.size() > 0:
		showing = true
		var tween = create_tween()
		await tween.tween_property($Point, "position", Vector2(-800,0),0.25).finished
		$Point/Message.text = "[center]%s[/center]" % messages[0]
		messages.remove_at(0)
		tween = create_tween()
		tween.tween_property($Point, "position", Vector2(800,0),0)
		tween.tween_property($Point, "position", Vector2(-40,0),0.5)
		await tween.tween_interval(2).finished
		showing = false
		
