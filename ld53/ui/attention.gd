extends Node2D

func _ready():
	
	var _tween = create_tween()
	_tween.tween_property($Sprite2D, "scale", Vector2(1.25,1.25), .5)
	_tween.tween_property($Sprite2D, "scale", Vector2.ONE, .5)
	_tween.set_loops()
	_tween.play()
