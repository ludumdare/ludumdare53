class_name CloudsSystem
extends System

@export var cloud_entity: PackedScene

func on_process_entity(entity : Entity, delta : float):
	
	var _tickaction = entity.get_component('tickaction') as TickActionComponent
	var _clouds = entity.get_component('clouds') as CloudsComponent
	
	if _clouds.first:
		
		var _count = randi_range(0, _clouds.max_pregen)
		for i in range(_count):
			
			var _cloud = cloud_entity.instantiate() as Area2D
			add_child(_cloud)
			var _xpos = randi_range(100, 860)
			_cloud.global_position += Vector2(_xpos, 0)

		_clouds.first = false
		
		return
		
	if _tickaction.has_action:
		
		Logger.trace("- spawn clouds")
		
		_tickaction.has_action = false
		
		var _count = randi_range(0, _clouds.max_clouds)
		for i in range(_count):
			
			var _cloud = cloud_entity.instantiate() as Area2D
			add_child(_cloud)
