class_name WorldSystem
extends System

func on_process_entity(entity :  Entity, delta : float):
	
	var _tickaction = entity.get_component("tickaction") as TickActionComponent
	
	if _tickaction.has_action:
		
		Global.update_world.emit()
		_tickaction.has_action = false
	
		
