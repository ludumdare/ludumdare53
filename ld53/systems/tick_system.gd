class_name TickSystem
extends System

@export var ticks_per_second : float = 60.0

func on_process_entity(entity :  Entity, delta : float):
	
	var _tick = entity.get_component("tick") as TickComponent
	
	_tick.ticks_per_second = ticks_per_second
	_tick.tick_count += ticks_per_second * delta
	
	Logger.fine('tick_count %s' % _tick.tick_count)
	
	if (_tick.tick_count >= 1.0):
		_tick.tick += 1
		_tick.tick_count = _tick.tick_count - 1.0
		Logger.fine("- tick %s" % [_tick.tick])
		
