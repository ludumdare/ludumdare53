extends System
class_name MovetoSystem

func on_process_entity(entity : Entity, delta : float):
	
	var _moveto = entity.get_component("moveto") as MovetoComponent
	var _move = entity.get_component("move") as MoveComponent
	
	if _moveto.moving:
		
		var dist = abs(entity.global_position.distance_to(_moveto.target))
		
		if dist < 1:
			_moveto.moving = false
			_moveto.complete = true
			return
			
		_move.direction = entity.global_position.direction_to(_moveto.target)
		
