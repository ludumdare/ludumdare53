extends System
class_name PigeonSystem

func on_process_entity(entity : Entity, delta : float):
	
	var _pigeon = entity.get_component("pigeon") as PigeonComponent
	var _action = entity.get_component("tickaction") as TickActionComponent
	var _moveto = entity.get_component("moveto") as MovetoComponent
	var _move = entity.get_component("move") as MoveComponent
	var _anim = entity.get_node("AnimatedSprite2D") as AnimatedSprite2D
	

	match _pigeon.state:
		
		_pigeon.PigeonStates.IDLE:
			_anim.play("idle")
			_action.ticks_per_action = 60
			
		_pigeon.PigeonStates.EAT:
			_anim.play("eat")
			_pigeon.state = 0
			return
			
		_pigeon.PigeonStates.TURN:
			entity.scale = entity.scale * Vector2(-1, 1)
			_pigeon.state = 0
			return
			
		_pigeon.PigeonStates.JUMP:
			_moveto.target = entity.global_position + (Vector2.UP * 30)
			var _xpos = randi_range(-20,20)
			_moveto.target += Vector2(_xpos, 0)
			_moveto.moving = true
			_pigeon.state = _pigeon.PigeonStates.JUMPING
			_anim.play("land")
			return
			
		_pigeon.PigeonStates.FLY:
			var x = randi_range(100,860)
			var y = randi_range(0,240)
			_moveto.target = Vector2(x,y)
			_moveto.moving = true
			_pigeon.state = _pigeon.PigeonStates.JUMPING
			_anim.play("land")
			return
			
		_pigeon.PigeonStates.STRUT:
			
			var _dirs = [ Vector2.LEFT, Vector2.RIGHT ]
			var _dirp = randi_range(0,1) 
			var _dir = _dirs[_dirp]
			_moveto.target = entity.global_position + ( _dir * 30 )
			entity.scale = Vector2(_dir.x * -1, 1)
			_moveto.moving = true
			_pigeon.state = _pigeon.PigeonStates.STRUTTING
			_anim.play("strut")
			return
			
		_pigeon.PigeonStates.LAND:
			# turn off actions until we land
			_action.ticks_per_action = 0
			_pigeon.state = _pigeon.PigeonStates.NONE
			_pigeon.grounded = true
			var _gravity = GravityComponent.new()
			_gravity.name = "Gravity"
			entity.add_component(_gravity)
			_anim.play("land")
			return
			
		_pigeon.PigeonStates.JUMPING:
			if _move.direction.x >0:
				entity.scale = Vector2(-1,1)
			else:
				entity.scale = Vector2(1,1)
			if _moveto.complete:
				_moveto.complete = false
				_moveto.target = Vector2.ZERO
				_move.direction = Vector2.ZERO
				_pigeon.state = _pigeon.PigeonStates.LAND
			return
				
		_pigeon.PigeonStates.STRUTTING:
			if _moveto.complete:
				_moveto.complete = false
				_moveto.target = Vector2.ZERO
				_move.direction = Vector2.ZERO
				_pigeon.state = _pigeon.PigeonStates.IDLE
			return
			
	if _action.has_action:
		
		var _actions = []
		
		for i in range(10):
			_actions.append(_pigeon.PigeonStates.IDLE)
		
		for i in range(10):
			_actions.append(_pigeon.PigeonStates.EAT)
		
		for i in range(10):
			_actions.append(_pigeon.PigeonStates.JUMP)
		
		for i in range(10):
			_actions.append(_pigeon.PigeonStates.FLY)
		
		for i in range(10):
			_actions.append(_pigeon.PigeonStates.STRUT)
		
		_action.has_action = false
		
		var _state = randi_range(0, _actions.size() - 1)
		_pigeon.state = _actions[_state]
		
