class_name GravitySystem
extends System

@export var gravity: Vector2 = Vector2.DOWN
@export var gravity_force: float = 40.0

func on_process_entity(entity : Entity, delta : float):
	
	var _move = entity.get_component("move") as MoveComponent
	_move.direction = gravity
	_move.speed = gravity_force
