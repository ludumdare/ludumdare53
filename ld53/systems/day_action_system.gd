class_name DayActionSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _tickaction = entity.get_component('tickaction') as TickActionComponent
	var _dayaction = entity.get_component('dayaction') as DayActionComponent

	if (_tickaction.has_action):
		
		_tickaction.has_action = false
		_dayaction.actions += 1
		
		
		if _dayaction.actions > _dayaction.actions_per_stage:
			
			_dayaction.actions = 0
			_dayaction.stage_of_day += 1
			Global.next_day_stage.emit(_dayaction.stage_of_day)
		
			# include full stage before day changes	
			if _dayaction.stage_of_day > (_dayaction.stages_per_day-1):
				
				_dayaction.stage_of_day = 0
				_dayaction.days += 1
				Global.next_day.emit(_dayaction.days)
		
