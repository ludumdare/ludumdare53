class_name MoveSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _move = entity.get_component("move") as MoveComponent
	
	entity.position += _move.direction * _move.speed * _move.speed_factor * delta
	
