class_name TickActionSystem
extends System

func on_process_entity(entity :  Entity, delta : float):
	
	var _tick = entity.get_component("tick") as TickComponent
	var _tickaction = entity.get_component("tickaction") as TickActionComponent
	
	if _tickaction.ticks_per_action <= 0:
		return
		
	if (_tick.tick != _tickaction.last_tick):
		_tickaction.last_tick = _tick.tick
		_tickaction.tick += 1
		Logger.fine("- action tick %s" % [_tickaction.tick])

	if (_tickaction.tick >= _tickaction.ticks_per_action):
		_tickaction.tick = 0
		_tickaction.actions += 1
		_tickaction.has_action = true
		Logger.fine("- action %s" % [_tickaction.actions])
