extends Node2D

var contracts = []
var index: int = 0

func _ready():
	
	Global.next_contract.connect(on_next_contract)
	Global.accept_contract.connect(on_accept_contract)
	
	for i in range(10):
		contracts.append(Global.create_contract())
	
#	var contract = ContractResource.new()
#	contract.pigeons = 10
#	contract.type = "Recon"
#	contract.distance = 10
#	contract.days_travel = 1
#	contract.days_to_expire = 20
#	contract.difficulty = 0
#	contract.payment = 25
#	contract.reward = 100
#	contract.victory_points = 1
#
#	contracts.append(contract)	
#
#	contract = ContractResource.new()
#	contract.pigeons = 25
#	contract.type = "Support"
#	contract.distance = 15
#	contract.days_travel = 2
#	contract.days_to_expire = 25
#	contract.difficulty = 1
#	contract.payment =50
#	contract.reward = 250
#	contract.victory_points = 2
#
#	contracts.append(contract)	
	
	Global.show_contract.emit(contracts[index])


func on_next_contract():
	index += 1
	
	if index > contracts.size() - 1:
		index = 0 
		
	Global.show_contract.emit(contracts[index], index, contracts.size())
	
func on_accept_contract():
	print("accept")

