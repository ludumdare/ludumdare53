extends Node2D

var __game_data: GameResource
var __intel_warning: bool = false
var __contract_warning: bool = true

func _ready():
	
	__game_data = GameResource.new()
	
	Global.contract_warning.connect(_on_contract_warning)
	Global.intel_warning.connect(_on_intel_warning)


func _on_timer_timeout():
	
	__game_data.pigeons = randi_range(0,500)
	__game_data.food = randi_range(0,500)
	__game_data.currency = randi_range(0,10000000)
	__game_data.contracts_available = randi_range(0,25)
	__game_data.contracts_active = randi_range(0,25)
	__game_data.contracts_success = randi_range(0,25)
	__game_data.contracts_failure = randi_range(0,25)
	__game_data.capacity = randi_range(0,500)
	__game_data.storage = randi_range(0,500)
	
	__intel_warning = ! __intel_warning
	__contract_warning = ! __contract_warning
	
	Global.update_ui.emit(__game_data)
	Global.intel_warning.emit(__intel_warning)
	Global.contract_warning.emit(__contract_warning)
	

func _on_contract_warning(state):
	$Contracts/Attention.visible = state
	
	
func _on_intel_warning(state):
	$Intel/Attention.visible = state
	
	
