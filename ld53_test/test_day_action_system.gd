extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	Global.next_day.connect(_on_next_day)
	Global.next_day_stage.connect(_on_next_day_stage)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	ECS.update()


func _on_next_day(day):
	Logger.trace("- day %d" % day)
	
func _on_next_day_stage(stage):
	Logger.trace("- stage %d" % stage)
	
