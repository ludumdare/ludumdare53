extends Node2D

func _ready():
	
	Global.update_world.connect(__on_world_update__)
	
	
func _process(delta):
	
	ECS.update() 
	
	
func __on_world_update__():
	
	Logger.debug("- updating world")
		
