extends Node

signal next_day(day)
signal next_day_stage(stage)
signal update_ui(game_data)
signal intel_warning(state)
signal contract_warning(state)
signal update_world()
signal exit_button()
signal next_contract()
signal accept_contract()
signal show_contract(contract: ContractResource, index, total, pigeons)
signal pigeons_passed(total)
signal pigeons_born(total)
signal contract_success(contract: ContractResource)
signal contract_failure(conract: ContractResource)
signal new_message(message: String)

var contract_data_filename = "res://ld53/data/contract_data.ini"
var contract_data: ConfigFile

var contract_difficulty = []
var contract_types = []
var contract_cities = []
var contract_pigeons = []
var contract_victory = []
var contract_reward = []
var contract_payment = []
var contract_distance = []


enum PlayStates
{
	INIT,			# init playing
	START,			# right before we start playing
	STOP,			# right after we are finished playing
	PLAY,			# playing the game
	IDLE,			# waiting for something
	CONTRACT,		# show contracts
	SHOP,			# purchase supplies
	MENU,			# menu
	QUIT,			# quit playing
	WIN,
	LOSE
}


func _ready():
	
	var _config = ConfigFile.new()
	
	Game.load_sounds($Sounds)
	Game.load_music($Music)
	
	if not FileAccess.file_exists(contract_data_filename):
		Logger.warn("Configuration File {name} is missing.".format({"name": contract_data_filename}))
		return

	var _err = _config.load(contract_data_filename)
	
	if (_err > 0):
		Logger.error("Error {0} Reading Configuration File.".format([_err]))
		return
		
	for _key in _config.get_section_keys("types"):
		contract_types.append(_config.get_value("types",_key))
		
	for _key in _config.get_section_keys("difficulty"):
		contract_difficulty.append(_config.get_value("difficulty",_key))
		
	for _key in _config.get_section_keys("pigeons"):
		contract_pigeons.append(_config.get_value("pigeons",_key))
		
	for _key in _config.get_section_keys("payment"):
		contract_payment.append(_config.get_value("payment",_key))
		
	for _key in _config.get_section_keys("victory"):
		contract_victory.append(_config.get_value("victory",_key))
		
	for _key in _config.get_section_keys("reward"):
		contract_reward.append(_config.get_value("reward",_key))
		
	for _key in _config.get_section_keys("distance"):
		contract_distance.append(_config.get_value("distance",_key))
		
	pass

	
	
func format_commas(value : String) -> String:
	var i : int = value.length() - 3
	while i > 0:
		value = value.insert(i, ",")
		i = i - 3
	return value
	

func create_contract(level=-1):
	
	var contract = ContractResource.new()
	var difficulty = level
	if difficulty <0:
		difficulty = randi_range(0,3)
	contract.type = get_random(contract_types)
	contract.difficulty = difficulty
	contract.difficulty_value = contract_difficulty[difficulty]
	contract.pigeons = get_range(contract_pigeons[difficulty])
	contract.pigeons_left = contract.pigeons
	contract.pigeons_sent = contract.pigeons
	contract.distance = get_frange(contract_distance[difficulty])
	contract.days_travel = int(contract.distance / 60) + 1
	contract.days_travel_left = contract.days_travel
	contract.days_return = int(contract.distance / 30) + 1
	contract.days_return_left = contract.days_return
	contract.payment = get_range(contract_payment[difficulty]) + (5 * difficulty)
	contract.reward = get_range(contract_reward[difficulty]) + (10 * difficulty)
	contract.victory_points = contract_victory[difficulty]
	contract.days_to_expire = (difficulty + 1) + 2
	return contract


func get_random(bucket: Array):
	
	var i = randi_range(0, bucket.size() - 1)
	return bucket[i]


func get_range(max):
	var val = randi_range(0,max) + 5
	var round = ceil(val/5)*5
	return round

func get_frange(max):
	var val = randi_range(0,max)
	return val
